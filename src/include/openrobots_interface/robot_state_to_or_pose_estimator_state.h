
#ifndef _ROBOT_STATE_TO_OR_POSE_ESTIMATOR_STATE_H
#define _ROBOT_STATE_TO_OR_POSE_ESTIMATOR_STATE_H


// String
#include <string>

// Eigen
#include <Eigen/Dense>

//
#include "quaternion_algebra/quaternion_algebra.h"

// Ros
#include "ros/ros.h"

// message filters
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>

// robot state
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include "geometry_msgs/AccelWithCovarianceStamped.h"

// pom/or_pose_estimator::state
#include "pom/or_pose_estimator_state.h"
#include "pom/or_time_ts.h"
#include "pom/or_t3d_pos.h"
#include "pom/or_t3d_pos_cov.h"
#include "pom/or_t3d_vel.h"
#include "pom/or_t3d_vel_cov.h"
#include "pom/or_t3d_acc.h"
#include "pom/or_t3d_acc_cov.h"


class RobotStateToOrPoseEstimatorState
{
public:
    RobotStateToOrPoseEstimatorState(int argc,char **argv);
    ~RobotStateToOrPoseEstimatorState();

protected:
    ros::NodeHandle* nh_;

    // Subscribed topic config
protected:
    int config_subscribed_topics_;

    // Intrinsic value
protected:
    bool intrinsic_flag_;

    // Subscriber for robot pose
protected:
    std::string robot_pose_with_covariance_stamped_topic_name_;
    //Subscriber
    message_filters::Subscriber<geometry_msgs::PoseWithCovarianceStamped>* robot_pose_with_covariance_stamped_subscriber_;

    // Subscriber for robot velocity
protected:
    std::string robot_velocity_with_covariance_stamped_topic_name_;
    //Subscriber
    message_filters::Subscriber<geometry_msgs::TwistWithCovarianceStamped>* robot_velocity_with_covariance_stamped_subscriber_;

    // Subscriber for robot acceleration
protected:
    std::string robot_accel_with_covariance_stamped_topic_name_;
    //Subscriber
    message_filters::Subscriber<geometry_msgs::AccelWithCovarianceStamped>* robot_accel_with_covariance_stamped_subscriber_;

    // Synchronization between robot state topics
protected:
    // Full State
    typedef message_filters::sync_policies::ExactTime<geometry_msgs::PoseWithCovarianceStamped, geometry_msgs::TwistWithCovarianceStamped, geometry_msgs::AccelWithCovarianceStamped> RobotStateFullSyncPolicy;
    message_filters::Synchronizer<RobotStateFullSyncPolicy>* messages_syncronizer_robot_state_full_;
    void robotStateFullCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& robot_pose,
                            const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& robot_velocity,
                            const geometry_msgs::AccelWithCovarianceStamped::ConstPtr& robot_accel);

    // Only Velocity and Acceleration
    typedef message_filters::sync_policies::ExactTime<geometry_msgs::TwistWithCovarianceStamped, geometry_msgs::AccelWithCovarianceStamped> RobotStateVelAccSyncPolicy;
    message_filters::Synchronizer<RobotStateVelAccSyncPolicy>* messages_syncronizer_robot_state_vel_acc_;
    void robotStateVelAccCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& robot_velocity,
                            const geometry_msgs::AccelWithCovarianceStamped::ConstPtr& robot_accel);

    // Publisher
protected:
    std::string pom_or_pose_estimation_state_topic_name_;
    ros::Publisher pom_or_pose_estimation_state_publisher_;
    pom::or_pose_estimator_state pom_or_pose_estimation_state_msg_;

    // Basic
public:
    void open();
    void run();


    // Conversions
protected:
    int geometry_msgs_pose_to_or_t3d_pos(const geometry_msgs::Pose& geometry_msgs_pose, pom::or_t3d_pos& or_t3d_pos_msg);
    int geometry_msgs_twist_to_or_t3d_vel(const geometry_msgs::Twist& geometry_msgs_twist, pom::or_t3d_vel& or_t3d_vel_msg);
    int geometry_msgs_accel_to_or_t3d_acc(const geometry_msgs::Accel& geometry_msgs_accel, pom::or_t3d_acc& or_t3d_acc_msg);

    int pose_cov_to_or_t3d_pos_cov(const geometry_msgs::PoseWithCovariance& geometry_msgs_pose_with_covariance, pom::or_t3d_pos_cov& or_t3d_pos_cov_msg);
    int twist_cov_to_or_t3d_vel_cov(const geometry_msgs::TwistWithCovariance& geometry_msgs_twist_covariance, pom::or_t3d_vel_cov& or_t3d_vel_cov_msg);
    int accel_cov_to_or_t3d_acc_cov(const geometry_msgs::AccelWithCovariance& geometry_msgs_accel_covariance, pom::or_t3d_acc_cov& or_t3d_acc_cov_msg);

};








#endif
