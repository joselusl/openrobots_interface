
#ifndef _BASIC_IMU_ROBOT_STATE_ESTIMATOR_H
#define _BASIC_IMU_ROBOT_STATE_ESTIMATOR_H


// String
#include <string>

// Math
#include <cmath>

// Limits
#include <limits>

// Eigen
#include <Eigen/Dense>

// Ros
#include "ros/ros.h"

// sensor_msgs/imu Message
#include "sensor_msgs/Imu.h"

// robot state
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include "geometry_msgs/AccelWithCovarianceStamped.h"


class BasicImuRobotStateEstimator
{
public:
    BasicImuRobotStateEstimator(int argc,char **argv);
    ~BasicImuRobotStateEstimator();

protected:
    ros::NodeHandle* nh_;

    // Subscriber
protected:
    std::string sensor_msgs_imu_topic_name_;
    ros::Subscriber sensor_msgs_imu_subscriber_;
public:
    void sensorMsgsImuCallback(const sensor_msgs::Imu::ConstPtr& msg);


    // Publisher Robot Velocity
protected:
    std::string robot_velocity_topic_name_;
    ros::Publisher robot_velocity_state_publisher_;
    geometry_msgs::TwistWithCovarianceStamped robot_velocity_msg_;

    // Publisher Robot Acceleration Specific
protected:
    std::string robot_acceleration_topic_name_;
    ros::Publisher robot_acceleration_state_publisher_;
    geometry_msgs::AccelWithCovarianceStamped robot_acceleration_msg_;

    // Basic
public:
    void open();
    void run();

    // Imu parameters
protected:
    Eigen::Matrix3d sensitivity_lin_acc_;
    Eigen::Vector3d bias_lin_acc_;
    Eigen::Matrix3d covariance_lin_acc_;

protected:
    Eigen::Matrix3d sensitivity_ang_vel_;
    Eigen::Vector3d bias_ang_vel_;
    Eigen::Matrix3d covariance_ang_vel_;

};



#endif
