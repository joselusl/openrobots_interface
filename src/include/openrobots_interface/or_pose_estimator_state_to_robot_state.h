
#ifndef _OR_POSE_ESTIMATOR_STATE_TO_ROBOT_STATE_H
#define _OR_POSE_ESTIMATOR_STATE_TO_ROBOT_STATE_H


// String
#include <string>

// Eigen
#include <Eigen/Dense>

//
#include "quaternion_algebra/quaternion_algebra.h"

// Ros
#include "ros/ros.h"

// tf
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

// robot state
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include "geometry_msgs/AccelWithCovarianceStamped.h"

// pom/or_pose_estimator::state
#include "pom/or_pose_estimator_state.h"
#include "pom/or_time_ts.h"
#include "pom/or_t3d_pos.h"
#include "pom/or_t3d_pos_cov.h"
#include "pom/or_t3d_vel.h"
#include "pom/or_t3d_vel_cov.h"
#include "pom/or_t3d_acc.h"
#include "pom/or_t3d_acc_cov.h"


class OrPoseEstimatorStateToRobotState
{
public:
    OrPoseEstimatorStateToRobotState(int argc,char **argv);
    ~OrPoseEstimatorStateToRobotState();

protected:
    ros::NodeHandle* nh_;

    // Ref Frames Names
protected:
    std::string child_ref_frame_name_;
    std::string parent_ref_frame_name_;

    // Tf
protected:
    tf::TransformBroadcaster* tf_transform_broadcaster_;

    // Publisher for robot pose
protected:
    std::string robot_pose_with_covariance_stamped_topic_name_;
    // Publisher
    ros::Publisher robot_pose_with_covariance_stamped_pub_;
    geometry_msgs::PoseWithCovarianceStamped robot_pose_with_covariance_stamped_msg_;

    // Publisher for robot velocity
protected:
    std::string robot_velocity_with_covariance_stamped_topic_name_;
    // Publisher
    ros::Publisher robot_velocity_with_covariance_stamped_pub_;
    geometry_msgs::TwistWithCovarianceStamped robot_velocity_with_covariance_stamped_msg_;

    // Publisher for robot acceleration
protected:
    std::string robot_accel_with_covariance_stamped_topic_name_;
    // Publisher
    ros::Publisher robot_accel_with_covariance_stamped_pub_;
    geometry_msgs::AccelWithCovarianceStamped robot_accel_with_covariance_stamped_msg_;


    // Subscriber
protected:
    std::string pom_or_pose_estimation_state_topic_name_;
    ros::Subscriber pom_or_pose_estimation_state_sub_;
    void pomOrPoseEstimationStateCallback(const pom::or_pose_estimator_state::ConstPtr& pom_or_pose_estimation_state_msg);

    // Basic
public:
    void open();
    void run();


    // Tf publish
protected:
    int publishTfPoseEstimated(const geometry_msgs::PoseWithCovarianceStamped& geometry_msgs_pose);


    // Conversions
protected:
    int or_t3d_pos_to_geometry_msgs_pose(const pom::or_t3d_pos& or_t3d_pos_msg, geometry_msgs::Pose& geometry_msgs_pose);
    int or_t3d_vel_to_geometry_msgs_twist(const pom::or_t3d_vel& or_t3d_vel_msg, geometry_msgs::Twist& geometry_msgs_twist);
    int or_t3d_acc_to_geometry_msgs_accel(const pom::or_t3d_acc& or_t3d_acc_msg, geometry_msgs::Accel& geometry_msgs_accel);

    int or_t3d_pos_cov_to_pose_cov(const pom::or_t3d_pos_cov& or_t3d_pos_cov_msg, geometry_msgs::PoseWithCovariance& geometry_msgs_pose_with_covariance);
    int or_t3d_vel_cov_to_twist_cov(const pom::or_t3d_vel_cov& or_t3d_vel_cov_msg, geometry_msgs::TwistWithCovariance& geometry_msgs_twist_covariance);
    int or_t3d_acc_cov_to_accel_cov(const pom::or_t3d_acc_cov& or_t3d_acc_cov_msg, geometry_msgs::AccelWithCovariance& geometry_msgs_accel_covariance);




};








#endif
