
#include "openrobots_interface/basic_imu_robot_state_estimator.h"



int main(int argc,char **argv)
{

    BasicImuRobotStateEstimator MyBasicImuRobotStateEstimator(argc, argv);
    std::cout<<"[ROSNODE] Starting "<<ros::this_node::getName()<<std::endl;

    MyBasicImuRobotStateEstimator.open();

    MyBasicImuRobotStateEstimator.run();

    return 0;
}
