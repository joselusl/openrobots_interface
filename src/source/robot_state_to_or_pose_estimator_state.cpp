
#include "openrobots_interface/robot_state_to_or_pose_estimator_state.h"

RobotStateToOrPoseEstimatorState::RobotStateToOrPoseEstimatorState(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Init values
    intrinsic_flag_=false;

    config_subscribed_topics_=0;

    // Node handle
    nh_=new ros::NodeHandle();
}

RobotStateToOrPoseEstimatorState::~RobotStateToOrPoseEstimatorState()
{
    // Delete
    delete nh_;

    delete messages_syncronizer_robot_state_full_;
    delete messages_syncronizer_robot_state_vel_acc_;

    delete robot_pose_with_covariance_stamped_subscriber_;
    delete robot_velocity_with_covariance_stamped_subscriber_;
    delete robot_accel_with_covariance_stamped_subscriber_;

    return;
}

void RobotStateToOrPoseEstimatorState::robotStateFullCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &robot_pose,
                                                            const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& robot_velocity,
                                                            const geometry_msgs::AccelWithCovarianceStamped::ConstPtr& robot_accel)
{
    // Convert

    // Time Stamp (from any. they must be the same)
    pom_or_pose_estimation_state_msg_.ts.sec=robot_pose->header.stamp.sec;
    pom_or_pose_estimation_state_msg_.ts.nsec=robot_pose->header.stamp.nsec;

    // Intrinsic flag (false if in world coordinates)
    pom_or_pose_estimation_state_msg_.intrinsic=intrinsic_flag_;

    // Pose
    pom_or_pose_estimation_state_msg_.pos.clear();
    pom::or_t3d_pos or_t3d_pos_msg;
    geometry_msgs_pose_to_or_t3d_pos(robot_pose->pose.pose, or_t3d_pos_msg);
    pom_or_pose_estimation_state_msg_.pos.push_back(or_t3d_pos_msg);

    // Pose Covariance
    pom_or_pose_estimation_state_msg_.pos_cov.clear();
    pom::or_t3d_pos_cov or_t3d_pos_cov_msg;
    pose_cov_to_or_t3d_pos_cov(robot_pose->pose, or_t3d_pos_cov_msg);
    pom_or_pose_estimation_state_msg_.pos_cov.push_back(or_t3d_pos_cov_msg);

    // Velocity
    pom_or_pose_estimation_state_msg_.vel.clear();
    pom::or_t3d_vel or_t3d_vel_msg;
    geometry_msgs_twist_to_or_t3d_vel(robot_velocity->twist.twist, or_t3d_vel_msg);
    pom_or_pose_estimation_state_msg_.vel.push_back(or_t3d_vel_msg);

    // Velocity Covariance
    pom_or_pose_estimation_state_msg_.vel_cov.clear();
    pom::or_t3d_vel_cov or_t3d_vel_cov_msg;
    twist_cov_to_or_t3d_vel_cov(robot_velocity->twist, or_t3d_vel_cov_msg);
    pom_or_pose_estimation_state_msg_.vel_cov.push_back(or_t3d_vel_cov_msg);

    // Acceleration
    pom_or_pose_estimation_state_msg_.acc.clear();
    pom::or_t3d_acc or_t3d_acc_msg;
    geometry_msgs_accel_to_or_t3d_acc(robot_accel->accel.accel, or_t3d_acc_msg);
    pom_or_pose_estimation_state_msg_.acc.push_back(or_t3d_acc_msg);

    // Acceleration Covariance
    pom_or_pose_estimation_state_msg_.acc_cov.clear();
    pom::or_t3d_acc_cov or_t3d_acc_cov_msg;
    accel_cov_to_or_t3d_acc_cov(robot_accel->accel, or_t3d_acc_cov_msg);
    pom_or_pose_estimation_state_msg_.acc_cov.push_back(or_t3d_acc_cov_msg);


    // Publish
    pom_or_pose_estimation_state_publisher_.publish(pom_or_pose_estimation_state_msg_);

    // End
    return;
}

void RobotStateToOrPoseEstimatorState::robotStateVelAccCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& robot_velocity,
                                                        const geometry_msgs::AccelWithCovarianceStamped::ConstPtr& robot_accel)
{
    // Convert

    // Time Stamp (from any. they must be the same)
    pom_or_pose_estimation_state_msg_.ts.sec=robot_velocity->header.stamp.sec;
    pom_or_pose_estimation_state_msg_.ts.nsec=robot_velocity->header.stamp.nsec;

    // Intrinsic flag
    pom_or_pose_estimation_state_msg_.intrinsic=intrinsic_flag_;

    // Pose
    pom_or_pose_estimation_state_msg_.pos.clear();

    // Pose Covariance
    pom_or_pose_estimation_state_msg_.pos_cov.clear();

    // Velocity
    pom_or_pose_estimation_state_msg_.vel.clear();
    pom::or_t3d_vel or_t3d_vel_msg;
    geometry_msgs_twist_to_or_t3d_vel(robot_velocity->twist.twist, or_t3d_vel_msg);
    pom_or_pose_estimation_state_msg_.vel.push_back(or_t3d_vel_msg);

    // Velocity Covariance
    pom_or_pose_estimation_state_msg_.vel_cov.clear();
    pom::or_t3d_vel_cov or_t3d_vel_cov_msg;
    twist_cov_to_or_t3d_vel_cov(robot_velocity->twist, or_t3d_vel_cov_msg);
    pom_or_pose_estimation_state_msg_.vel_cov.push_back(or_t3d_vel_cov_msg);

    // Acceleration
    pom_or_pose_estimation_state_msg_.acc.clear();
    pom::or_t3d_acc or_t3d_acc_msg;
    geometry_msgs_accel_to_or_t3d_acc(robot_accel->accel.accel, or_t3d_acc_msg);
    pom_or_pose_estimation_state_msg_.acc.push_back(or_t3d_acc_msg);

    // Acceleration Covariance
    pom_or_pose_estimation_state_msg_.acc_cov.clear();
    pom::or_t3d_acc_cov or_t3d_acc_cov_msg;
    accel_cov_to_or_t3d_acc_cov(robot_accel->accel, or_t3d_acc_cov_msg);
    pom_or_pose_estimation_state_msg_.acc_cov.push_back(or_t3d_acc_cov_msg);


    // Publish
    pom_or_pose_estimation_state_publisher_.publish(pom_or_pose_estimation_state_msg_);

    // End
    return;
}

void RobotStateToOrPoseEstimatorState::open()
{
    // Read parameters
    //
    ros::param::param<int>("~config_subscribed_topics", config_subscribed_topics_, 0);
    std::cout<<"config_subscribed_topics="<<config_subscribed_topics_<<std::endl;
    //
    ros::param::param<bool>("~intrinsic_flag", intrinsic_flag_, false);
    std::cout<<"intrinsic_flag="<<intrinsic_flag_<<std::endl;
    //
    ros::param::param<std::string>("~robot_pose_with_covariance_stamped_topic_name", robot_pose_with_covariance_stamped_topic_name_, "msf_localization/robot_pose_cov");
    std::cout<<"robot_pose_with_covariance_stamped_topic_name="<<robot_pose_with_covariance_stamped_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_velocity_with_covariance_stamped_topic_name", robot_velocity_with_covariance_stamped_topic_name_, "msf_localization/robot_velocity_cov");
    std::cout<<"robot_velocity_with_covariance_stamped_topic_name="<<robot_velocity_with_covariance_stamped_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_accel_with_covariance_stamped_topic_name", robot_accel_with_covariance_stamped_topic_name_, "msf_localization/robot_acceleration_cov");
    std::cout<<"robot_accel_with_covariance_stamped_topic_name="<<robot_accel_with_covariance_stamped_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~pom_or_pose_estimation_state_topic_name", pom_or_pose_estimation_state_topic_name_, "pom/imu/data_raw");
    std::cout<<"pom_or_pose_estimation_state_topic_name="<<pom_or_pose_estimation_state_topic_name_<<std::endl;


    // Subscribers

    // Subscriber robot pose
    robot_pose_with_covariance_stamped_subscriber_=new message_filters::Subscriber<geometry_msgs::PoseWithCovarianceStamped>();
    robot_pose_with_covariance_stamped_subscriber_->subscribe(*nh_, robot_pose_with_covariance_stamped_topic_name_, 10);

    // Subscriber robot velocity
    robot_velocity_with_covariance_stamped_subscriber_=new message_filters::Subscriber<geometry_msgs::TwistWithCovarianceStamped>();
    robot_velocity_with_covariance_stamped_subscriber_->subscribe(*nh_, robot_velocity_with_covariance_stamped_topic_name_, 10);

    // Subscriber robot acceleration
    robot_accel_with_covariance_stamped_subscriber_=new message_filters::Subscriber<geometry_msgs::AccelWithCovarianceStamped>();
    robot_accel_with_covariance_stamped_subscriber_->subscribe(*nh_, robot_accel_with_covariance_stamped_topic_name_, 10);

    // messages Syncronizer
    switch(config_subscribed_topics_)
    {
    case 111: // 111
        {
            // Robot State Full
            messages_syncronizer_robot_state_full_=new message_filters::Synchronizer<RobotStateFullSyncPolicy>(RobotStateFullSyncPolicy( 10 ));
            messages_syncronizer_robot_state_full_->connectInput(*robot_pose_with_covariance_stamped_subscriber_, *robot_velocity_with_covariance_stamped_subscriber_, *robot_accel_with_covariance_stamped_subscriber_);
            messages_syncronizer_robot_state_full_->registerCallback( boost::bind( &RobotStateToOrPoseEstimatorState::robotStateFullCallback, this, _1, _2, _3 ) );
            ROS_INFO("Subscribed to Full State");
        }
        break;
    case 11: // 011
        {
            // Robot State Vel and Acc
            messages_syncronizer_robot_state_vel_acc_=new message_filters::Synchronizer<RobotStateVelAccSyncPolicy>(RobotStateVelAccSyncPolicy( 10 ));
            messages_syncronizer_robot_state_vel_acc_->connectInput(*robot_velocity_with_covariance_stamped_subscriber_, *robot_accel_with_covariance_stamped_subscriber_);
            messages_syncronizer_robot_state_vel_acc_->registerCallback( boost::bind( &RobotStateToOrPoseEstimatorState::robotStateVelAccCallback, this, _1, _2 ) );
            ROS_INFO("Subscribed to Velocity and Acceleration");
        }
        break;
    default:
        ROS_ERROR("ERROR with subscribed state!!");
    }


    // Publishers
    pom_or_pose_estimation_state_publisher_=nh_->advertise<pom::or_pose_estimator_state>(pom_or_pose_estimation_state_topic_name_,  1, true);

    return;
}

void RobotStateToOrPoseEstimatorState::run()
{
    ros::spin();
    return;
}

int RobotStateToOrPoseEstimatorState::geometry_msgs_pose_to_or_t3d_pos(const geometry_msgs::Pose& geometry_msgs_pose, pom::or_t3d_pos& or_t3d_pos_msg)
{
    // Position
    or_t3d_pos_msg.x=geometry_msgs_pose.position.x;
    or_t3d_pos_msg.y=geometry_msgs_pose.position.y;
    or_t3d_pos_msg.z=geometry_msgs_pose.position.z;
    // Attitude
    or_t3d_pos_msg.qx=geometry_msgs_pose.orientation.x;
    or_t3d_pos_msg.qy=geometry_msgs_pose.orientation.y;
    or_t3d_pos_msg.qz=geometry_msgs_pose.orientation.z;
    or_t3d_pos_msg.qw=geometry_msgs_pose.orientation.w;

    return 0;
}

int RobotStateToOrPoseEstimatorState::geometry_msgs_twist_to_or_t3d_vel(const geometry_msgs::Twist& geometry_msgs_twist, pom::or_t3d_vel& or_t3d_vel_msg)
{
    // Linear
    or_t3d_vel_msg.vx=geometry_msgs_twist.linear.x;
    or_t3d_vel_msg.vy=geometry_msgs_twist.linear.y;
    or_t3d_vel_msg.vz=geometry_msgs_twist.linear.z;
    // Angular
    or_t3d_vel_msg.wx=geometry_msgs_twist.angular.x;
    or_t3d_vel_msg.wy=geometry_msgs_twist.angular.y;
    or_t3d_vel_msg.wz=geometry_msgs_twist.angular.z;

    return 0;
}

int RobotStateToOrPoseEstimatorState::geometry_msgs_accel_to_or_t3d_acc(const geometry_msgs::Accel& geometry_msgs_accel, pom::or_t3d_acc& or_t3d_acc_msg)
{
    // Linear
    or_t3d_acc_msg.ax=geometry_msgs_accel.linear.x;
    or_t3d_acc_msg.ay=geometry_msgs_accel.linear.y;
    or_t3d_acc_msg.az=geometry_msgs_accel.linear.z;
    // Angular
    // Not needed

    return 0;
}

int RobotStateToOrPoseEstimatorState::pose_cov_to_or_t3d_pos_cov(const geometry_msgs::PoseWithCovariance& geometry_msgs_pose_with_covariance, pom::or_t3d_pos_cov& or_t3d_pos_cov_msg)
{
    // Full 6d pose cov mat
    Eigen::Matrix<double, 6, 6> pose_covariance_mat(geometry_msgs_pose_with_covariance.covariance.data());

    // Convert to 7d pose cov mat
    Eigen::Matrix<double, 7, 7> pose7d_covariance_mat;
    pose7d_covariance_mat.setZero();
    Eigen::Vector4d attitude;
    attitude<<geometry_msgs_pose_with_covariance.pose.orientation.w, geometry_msgs_pose_with_covariance.pose.orientation.x, geometry_msgs_pose_with_covariance.pose.orientation.y, geometry_msgs_pose_with_covariance.pose.orientation.z;

    Eigen::Matrix<double, 7, 6> jacobian_pose7d_wrt_pose6d;
    jacobian_pose7d_wrt_pose6d.setZero();
    jacobian_pose7d_wrt_pose6d.block<3,3>(0,0)=Eigen::Matrix3d::Identity();
    jacobian_pose7d_wrt_pose6d.block<4,3>(3,3)=Quaternion::quatMatPlus(attitude)*Quaternion::jacobians.mat_diff_error_quat_wrt_error_theta_sparse;
    pose7d_covariance_mat=jacobian_pose7d_wrt_pose6d*pose_covariance_mat*jacobian_pose7d_wrt_pose6d.transpose();

    // Low diagonal part of the 7d pose cov mat
    int elem_i=0;
    for(int row_i=0; row_i<7; row_i++)
    {
        for(int col_i=0; col_i<=row_i; col_i++)
        {
            or_t3d_pos_cov_msg.cov[elem_i]=pose7d_covariance_mat(row_i, col_i);
            elem_i++;
        }
    }

    return 0;
}

int RobotStateToOrPoseEstimatorState::twist_cov_to_or_t3d_vel_cov(const geometry_msgs::TwistWithCovariance& geometry_msgs_twist_covariance, pom::or_t3d_vel_cov& or_t3d_vel_cov_msg)
{
    // Full velocity covariance matrix
    Eigen::Matrix<double, 6, 6> twist_covariance_mat(geometry_msgs_twist_covariance.covariance.data());

    // Low diagonal part of the velocity cov mat
    int elem_i=0;
    for(int row_i=0; row_i<6; row_i++)
    {
        for(int col_i=0; col_i<=row_i; col_i++)
        {
            or_t3d_vel_cov_msg.cov[elem_i]=twist_covariance_mat(row_i, col_i);
            elem_i++;
        }
    }

    return 0;
}

int RobotStateToOrPoseEstimatorState::accel_cov_to_or_t3d_acc_cov(const geometry_msgs::AccelWithCovariance& geometry_msgs_accel_covariance, pom::or_t3d_acc_cov& or_t3d_acc_cov_msg)
{
    // Full accel covariance matrix
    Eigen::Matrix<double, 6, 6> accel_covariance_mat(geometry_msgs_accel_covariance.covariance.data());

    // Linear accel cov matrix
    Eigen::Matrix<double, 3, 3> linear_accel_covariance_mat=accel_covariance_mat.block<3, 3>(0, 0);

    // Low diagonal part of the linear accel cov mat
    int elem_i=0;
    for(int row_i=0; row_i<3; row_i++)
    {
        for(int col_i=0; col_i<=row_i; col_i++)
        {
            or_t3d_acc_cov_msg.cov[elem_i]=linear_accel_covariance_mat(row_i, col_i);
            elem_i++;
        }
    }

    return 0;
}
