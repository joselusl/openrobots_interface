
#include "openrobots_interface/robot_state_to_or_pose_estimator_state.h"


int main(int argc,char **argv)
{

    RobotStateToOrPoseEstimatorState MyRobotStateToOrPoseEstimatorState(argc, argv);
    std::cout<<"[ROSNODE] Starting "<<ros::this_node::getName()<<std::endl;

    MyRobotStateToOrPoseEstimatorState.open();

    MyRobotStateToOrPoseEstimatorState.run();

    return 0;
}

