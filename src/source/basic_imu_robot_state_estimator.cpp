
#include "openrobots_interface/basic_imu_robot_state_estimator.h"


BasicImuRobotStateEstimator::BasicImuRobotStateEstimator(int argc, char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();


    // Imu Parameters
    sensitivity_lin_acc_=Eigen::Matrix3d::Identity();
    bias_lin_acc_.setZero();
    covariance_lin_acc_<<1e-2,  0,      0,
                            0,  1e-2,   0,
                            0,  0,      1e-2;

    sensitivity_ang_vel_=Eigen::Matrix3d::Identity();
    bias_ang_vel_.setZero();
    covariance_ang_vel_<<1e-4,  0,      0,
                            0,  1e-4,   0,
                            0,  0,      1e-4;


    return;
}

BasicImuRobotStateEstimator::~BasicImuRobotStateEstimator()
{
    delete nh_;
    return;
}

void BasicImuRobotStateEstimator::sensorMsgsImuCallback(const sensor_msgs::Imu::ConstPtr &msg)
{
    // Fill messages

    // Velocity

    // Time Stamp
    robot_velocity_msg_.header.stamp.sec=msg->header.stamp.sec;
    robot_velocity_msg_.header.stamp.nsec=msg->header.stamp.nsec;

    // Linear Velocity
    robot_velocity_msg_.twist.twist.linear.x=std::numeric_limits<double>::quiet_NaN();
    robot_velocity_msg_.twist.twist.linear.y=std::numeric_limits<double>::quiet_NaN();
    robot_velocity_msg_.twist.twist.linear.z=std::numeric_limits<double>::quiet_NaN();

    // Angular Velocity
    Eigen::Vector3d angular_velocity;
    Eigen::Vector3d measured_angular_velocity;
    measured_angular_velocity<<msg->angular_velocity.x, msg->angular_velocity.y, msg->angular_velocity.z;
    angular_velocity=sensitivity_ang_vel_.inverse()*(measured_angular_velocity-bias_ang_vel_);
    robot_velocity_msg_.twist.twist.angular.x=angular_velocity(0);
    robot_velocity_msg_.twist.twist.angular.y=angular_velocity(1);
    robot_velocity_msg_.twist.twist.angular.z=angular_velocity(2);

    // Covariance
    Eigen::MatrixXd covariance_velocity(6,6);
    covariance_velocity.setZero();
    covariance_velocity.block<3,3>(3,3)=covariance_ang_vel_;
    double covariance_vel_array[36];
    Eigen::Map<Eigen::MatrixXd>(covariance_vel_array, 6, 6) = covariance_velocity;
    for(unsigned int i=0; i<36; i++)
    {
        robot_velocity_msg_.twist.covariance[i]=covariance_vel_array[i];
    }


    // Acceleration

    // Time Stamp
    robot_acceleration_msg_.header.stamp.sec=msg->header.stamp.sec;
    robot_acceleration_msg_.header.stamp.nsec=msg->header.stamp.nsec;

    // Linear Acceleration
    Eigen::Vector3d linear_accel;
    Eigen::Vector3d measured_linear_accel;
    measured_linear_accel<<msg->linear_acceleration.x, msg->linear_acceleration.y, msg->linear_acceleration.z;
    linear_accel=sensitivity_lin_acc_.inverse()*(measured_linear_accel-bias_lin_acc_);
    robot_acceleration_msg_.accel.accel.linear.x=linear_accel(0);
    robot_acceleration_msg_.accel.accel.linear.y=linear_accel(1);
    robot_acceleration_msg_.accel.accel.linear.z=linear_accel(2);

    // Angular Acceleration
    robot_acceleration_msg_.accel.accel.angular.x=std::numeric_limits<double>::quiet_NaN();
    robot_acceleration_msg_.accel.accel.angular.y=std::numeric_limits<double>::quiet_NaN();
    robot_acceleration_msg_.accel.accel.angular.z=std::numeric_limits<double>::quiet_NaN();

    // Covariance
    Eigen::MatrixXd covariance_acceleration(6,6);
    covariance_acceleration.setZero();
    covariance_acceleration.block<3,3>(0,0)=covariance_lin_acc_;
    double covariance_acc_array[36];
    Eigen::Map<Eigen::MatrixXd>(covariance_acc_array, 6, 6) = covariance_acceleration;
    for(unsigned int i=0; i<36; i++)
    {
        robot_acceleration_msg_.accel.covariance[i]=covariance_acc_array[i];
    }


    // Publish
    robot_velocity_state_publisher_.publish(robot_velocity_msg_);
    robot_acceleration_state_publisher_.publish(robot_acceleration_msg_);


    // End
    return;
}

void BasicImuRobotStateEstimator::open()
{
    // Read parameters
    //
    ros::param::param<std::string>("~sensor_msgs_imu_topic_name", sensor_msgs_imu_topic_name_, "phidgets_imu_1044/imu/data_raw");
    std::cout<<"sensor_msgs_imu_topic_name="<<sensor_msgs_imu_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_velocity_topic_name", robot_velocity_topic_name_, "phidgets_imu_1044/imu/estim_vel_cov");
    std::cout<<"robot_velocity_topic_name="<<robot_velocity_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_acceleration_topic_name", robot_acceleration_topic_name_, "phidgets_imu_1044/imu/estim_acc_cov");
    std::cout<<"robot_acceleration_topic_name="<<robot_acceleration_topic_name_<<std::endl;


    // Subscribers
    sensor_msgs_imu_subscriber_=nh_->subscribe(sensor_msgs_imu_topic_name_, 10, &BasicImuRobotStateEstimator::sensorMsgsImuCallback, this);

    // Publishers
    robot_velocity_state_publisher_=nh_->advertise<geometry_msgs::TwistWithCovarianceStamped>(robot_velocity_topic_name_,  1, true);
    robot_acceleration_state_publisher_=nh_->advertise<geometry_msgs::AccelWithCovarianceStamped>(robot_acceleration_topic_name_,  1, true);

    return;
}

void BasicImuRobotStateEstimator::run()
{
    ros::spin();
    return;
}
