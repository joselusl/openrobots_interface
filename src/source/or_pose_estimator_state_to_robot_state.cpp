
#include "openrobots_interface/or_pose_estimator_state_to_robot_state.h"

OrPoseEstimatorStateToRobotState::OrPoseEstimatorStateToRobotState(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());


    // Node handle
    nh_=new ros::NodeHandle();

    // tf broadcaster
    tf_transform_broadcaster_=new tf::TransformBroadcaster;

    return;
}

OrPoseEstimatorStateToRobotState::~OrPoseEstimatorStateToRobotState()
{
    // Delete
    delete nh_;

    return;
}

void OrPoseEstimatorStateToRobotState::pomOrPoseEstimationStateCallback(const pom::or_pose_estimator_state::ConstPtr& pom_or_pose_estimation_state_msg)
{
    // Check flag intrinsic
    if(pom_or_pose_estimation_state_msg->intrinsic==true)
    {
        ROS_INFO("POM Estimation in robot coordinates!!");
        return;
    }

    // Pose
    bool flag_pose_estim=false;
    if(!pom_or_pose_estimation_state_msg->pos.empty())
        flag_pose_estim=true;

    if(flag_pose_estim)
    {
        // Time Stamp
        robot_pose_with_covariance_stamped_msg_.header.stamp.sec=pom_or_pose_estimation_state_msg->ts.sec;
        robot_pose_with_covariance_stamped_msg_.header.stamp.nsec=pom_or_pose_estimation_state_msg->ts.nsec;

        // Parent Frame
        robot_pose_with_covariance_stamped_msg_.header.frame_id=parent_ref_frame_name_;

        // Values
        or_t3d_pos_to_geometry_msgs_pose(pom_or_pose_estimation_state_msg->pos[0], robot_pose_with_covariance_stamped_msg_.pose.pose);

        // Covariances
        if(!pom_or_pose_estimation_state_msg->pos_cov.empty())
            or_t3d_pos_cov_to_pose_cov(pom_or_pose_estimation_state_msg->pos_cov[0], robot_pose_with_covariance_stamped_msg_.pose);
    }

    // Velocity
    bool flag_vel_estim=false;
    if(!pom_or_pose_estimation_state_msg->vel.empty())
        flag_vel_estim=true;

    if(flag_vel_estim)
    {
        // Time Stamp
        robot_velocity_with_covariance_stamped_msg_.header.stamp.sec=pom_or_pose_estimation_state_msg->ts.sec;
        robot_velocity_with_covariance_stamped_msg_.header.stamp.nsec=pom_or_pose_estimation_state_msg->ts.nsec;

        // Parent Frame
        robot_velocity_with_covariance_stamped_msg_.header.frame_id=parent_ref_frame_name_;

        // Values
        or_t3d_vel_to_geometry_msgs_twist(pom_or_pose_estimation_state_msg->vel[0], robot_velocity_with_covariance_stamped_msg_.twist.twist);

        // Covariances
        if(!pom_or_pose_estimation_state_msg->vel_cov.empty())
            or_t3d_vel_cov_to_twist_cov(pom_or_pose_estimation_state_msg->vel_cov[0], robot_velocity_with_covariance_stamped_msg_.twist);
    }

    // Acceleration
    bool flag_acc_estim=false;
    if(!pom_or_pose_estimation_state_msg->acc.empty())
        flag_acc_estim=true;

    if(flag_acc_estim)
    {
        // Time Stamp
        robot_accel_with_covariance_stamped_msg_.header.stamp.sec=pom_or_pose_estimation_state_msg->ts.sec;
        robot_accel_with_covariance_stamped_msg_.header.stamp.nsec=pom_or_pose_estimation_state_msg->ts.nsec;

        // Parent Frame
        robot_accel_with_covariance_stamped_msg_.header.frame_id=parent_ref_frame_name_;

        // Values
        or_t3d_acc_to_geometry_msgs_accel(pom_or_pose_estimation_state_msg->acc[0], robot_accel_with_covariance_stamped_msg_.accel.accel);

        // Covariances
        if(!pom_or_pose_estimation_state_msg->acc_cov.empty())
            or_t3d_acc_cov_to_accel_cov(pom_or_pose_estimation_state_msg->acc_cov[0], robot_accel_with_covariance_stamped_msg_.accel);
    }


    // Publish
    if(flag_pose_estim)
    {
        robot_pose_with_covariance_stamped_pub_.publish(robot_pose_with_covariance_stamped_msg_);
        publishTfPoseEstimated(robot_pose_with_covariance_stamped_msg_);
    }
    if(flag_vel_estim)
        robot_velocity_with_covariance_stamped_pub_.publish(robot_velocity_with_covariance_stamped_msg_);
    if(flag_acc_estim)
        robot_accel_with_covariance_stamped_pub_.publish(robot_accel_with_covariance_stamped_msg_);


    return;
}

void OrPoseEstimatorStateToRobotState::open()
{
    // Read parameters
    //
    ros::param::param<std::string>("~child_ref_frame_name", child_ref_frame_name_, "pom_robot");
    std::cout<<"child_ref_frame_name="<<child_ref_frame_name_<<std::endl;
    //
    ros::param::param<std::string>("~parent_ref_frame_name", parent_ref_frame_name_, "world");
    std::cout<<"parent_ref_frame_name="<<parent_ref_frame_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_pose_with_covariance_stamped_topic_name", robot_pose_with_covariance_stamped_topic_name_, "msf_localization/robot_pose_cov");
    std::cout<<"robot_pose_with_covariance_stamped_topic_name="<<robot_pose_with_covariance_stamped_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_velocity_with_covariance_stamped_topic_name", robot_velocity_with_covariance_stamped_topic_name_, "msf_localization/robot_velocity_cov");
    std::cout<<"robot_velocity_with_covariance_stamped_topic_name="<<robot_velocity_with_covariance_stamped_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~robot_accel_with_covariance_stamped_topic_name", robot_accel_with_covariance_stamped_topic_name_, "msf_localization/robot_acceleration_cov");
    std::cout<<"robot_accel_with_covariance_stamped_topic_name="<<robot_accel_with_covariance_stamped_topic_name_<<std::endl;
    //
    ros::param::param<std::string>("~pom_or_pose_estimation_state_topic_name", pom_or_pose_estimation_state_topic_name_, "pom/imu/data_raw");
    std::cout<<"pom_or_pose_estimation_state_topic_name="<<pom_or_pose_estimation_state_topic_name_<<std::endl;


    // Publishers

    // Publisher robot pose
    robot_pose_with_covariance_stamped_pub_=nh_->advertise<geometry_msgs::PoseWithCovarianceStamped>(robot_pose_with_covariance_stamped_topic_name_, 1, true);

    // Publisher robot velocity
    robot_velocity_with_covariance_stamped_pub_=nh_->advertise<geometry_msgs::TwistWithCovarianceStamped>(robot_velocity_with_covariance_stamped_topic_name_, 1, true);

    // Publisher robot acceleration
    robot_accel_with_covariance_stamped_pub_=nh_->advertise<geometry_msgs::AccelWithCovarianceStamped>(robot_accel_with_covariance_stamped_topic_name_, 1, true);


    // Subscriber
    pom_or_pose_estimation_state_sub_=nh_->subscribe(pom_or_pose_estimation_state_topic_name_, 10, &OrPoseEstimatorStateToRobotState::pomOrPoseEstimationStateCallback, this);

    return;
}

void OrPoseEstimatorStateToRobotState::run()
{
    ros::spin();
    return;
}

int OrPoseEstimatorStateToRobotState::publishTfPoseEstimated(const geometry_msgs::PoseWithCovarianceStamped &geometry_msgs_pose)
{

    tf::Quaternion tf_rot(geometry_msgs_pose.pose.pose.orientation.x, geometry_msgs_pose.pose.pose.orientation.y, geometry_msgs_pose.pose.pose.orientation.z, geometry_msgs_pose.pose.pose.orientation.w);
    tf::Vector3 tf_tran(geometry_msgs_pose.pose.pose.position.x, geometry_msgs_pose.pose.pose.position.y, geometry_msgs_pose.pose.pose.position.z);

    tf::Transform transform(tf_rot, tf_tran);

    tf_transform_broadcaster_->sendTransform(tf::StampedTransform(transform, geometry_msgs_pose.header.stamp,
                                          this->parent_ref_frame_name_, this->child_ref_frame_name_));


    return 0;
}

int OrPoseEstimatorStateToRobotState::or_t3d_pos_to_geometry_msgs_pose(const pom::or_t3d_pos& or_t3d_pos_msg, geometry_msgs::Pose& geometry_msgs_pose)
{
    // Position
    geometry_msgs_pose.position.x=or_t3d_pos_msg.x;
    geometry_msgs_pose.position.y=or_t3d_pos_msg.y;
    geometry_msgs_pose.position.z=or_t3d_pos_msg.z;
    // Attitude
    geometry_msgs_pose.orientation.x=or_t3d_pos_msg.qx;
    geometry_msgs_pose.orientation.y=or_t3d_pos_msg.qy;
    geometry_msgs_pose.orientation.z=or_t3d_pos_msg.qz;
    geometry_msgs_pose.orientation.w=or_t3d_pos_msg.qw;

    return 0;
}

int OrPoseEstimatorStateToRobotState::or_t3d_vel_to_geometry_msgs_twist(const pom::or_t3d_vel& or_t3d_vel_msg, geometry_msgs::Twist& geometry_msgs_twist)
{
    // Linear
    geometry_msgs_twist.linear.x=or_t3d_vel_msg.vx;
    geometry_msgs_twist.linear.y=or_t3d_vel_msg.vy;
    geometry_msgs_twist.linear.z=or_t3d_vel_msg.vz;
    // Angular
    geometry_msgs_twist.angular.x=or_t3d_vel_msg.wx;
    geometry_msgs_twist.angular.y=or_t3d_vel_msg.wy;
    geometry_msgs_twist.angular.z=or_t3d_vel_msg.wz;

    return 0;
}

int OrPoseEstimatorStateToRobotState::or_t3d_acc_to_geometry_msgs_accel(const pom::or_t3d_acc& or_t3d_acc_msg, geometry_msgs::Accel& geometry_msgs_accel)
{
    // Linear
    geometry_msgs_accel.linear.x=or_t3d_acc_msg.ax;
    geometry_msgs_accel.linear.y=or_t3d_acc_msg.ay;
    geometry_msgs_accel.linear.z=or_t3d_acc_msg.az;
    // Angular
    geometry_msgs_accel.angular.x=std::numeric_limits<double>::quiet_NaN();
    geometry_msgs_accel.angular.y=std::numeric_limits<double>::quiet_NaN();
    geometry_msgs_accel.angular.z=std::numeric_limits<double>::quiet_NaN();

    return 0;
}

int OrPoseEstimatorStateToRobotState::or_t3d_pos_cov_to_pose_cov(const pom::or_t3d_pos_cov& or_t3d_pos_cov_msg, geometry_msgs::PoseWithCovariance& geometry_msgs_pose_with_covariance)
{
    // Full 6d pose cov mat
    Eigen::Matrix<double, 6, 6> pose6d_covariance_mat;
    pose6d_covariance_mat.setZero();

    // Convert to 7d pose cov mat
    Eigen::Matrix<double, 7, 7> pose7d_covariance_mat;
    pose7d_covariance_mat.setZero();

    // Low diagonal part of the 7d pose cov mat
    int elem_i=0;
    for(int row_i=0; row_i<7; row_i++)
    {
        for(int col_i=0; col_i<=row_i; col_i++)
        {
            pose7d_covariance_mat(row_i, col_i)=or_t3d_pos_cov_msg.cov[elem_i];
            elem_i++;
        }
    }

    // Set Full 7d pose Cov Matrix
    pose7d_covariance_mat.triangularView<Eigen::Upper>() = pose7d_covariance_mat.transpose();

    // Attitude Ref
    Eigen::Vector4d attitude;
    attitude<<geometry_msgs_pose_with_covariance.pose.orientation.w, geometry_msgs_pose_with_covariance.pose.orientation.x, geometry_msgs_pose_with_covariance.pose.orientation.y, geometry_msgs_pose_with_covariance.pose.orientation.z;

    // Jacobian
    Eigen::Matrix<double, 6, 7> jacobian_pose6d_wrt_pose7d;
    jacobian_pose6d_wrt_pose7d.setZero();
    jacobian_pose6d_wrt_pose7d.block<3,3>(0,0)=Eigen::Matrix3d::Identity();
    jacobian_pose6d_wrt_pose7d.block<3,4>(3,3)=Quaternion::jacobians.mat_diff_error_theta_wrt_error_quat_sparse*Quaternion::quatMatPlus(Quaternion::inv(attitude));

    // Convert
    pose6d_covariance_mat=jacobian_pose6d_wrt_pose7d*pose7d_covariance_mat*jacobian_pose6d_wrt_pose7d.transpose();

    // Convert to ROS Message
    double covarianceArray[36];
    Eigen::Map<Eigen::MatrixXd>(covarianceArray, 6, 6) = pose6d_covariance_mat;
    for(unsigned int i=0; i<36; i++)
    {
        geometry_msgs_pose_with_covariance.covariance[i]=covarianceArray[i];
    }


    return 0;
}

int OrPoseEstimatorStateToRobotState::or_t3d_vel_cov_to_twist_cov(const pom::or_t3d_vel_cov& or_t3d_vel_cov_msg, geometry_msgs::TwistWithCovariance& geometry_msgs_twist_covariance)
{
    // Full velocity covariance matrix
    Eigen::Matrix<double, 6, 6> twist_covariance_mat;
    twist_covariance_mat.setZero();

    // Low diagonal part of the velocity cov mat
    int elem_i=0;
    for(int row_i=0; row_i<6; row_i++)
    {
        for(int col_i=0; col_i<=row_i; col_i++)
        {
            twist_covariance_mat(row_i, col_i)=or_t3d_vel_cov_msg.cov[elem_i];
            elem_i++;
        }
    }

    // Set Full Vel Cov Matrix
    twist_covariance_mat.triangularView<Eigen::Upper>() = twist_covariance_mat.transpose();

    // Convert to ROS Message
    double covarianceArray[36];
    Eigen::Map<Eigen::MatrixXd>(covarianceArray, 6, 6) = twist_covariance_mat;
    for(unsigned int i=0; i<36; i++)
    {
        geometry_msgs_twist_covariance.covariance[i]=covarianceArray[i];
    }


    return 0;
}

int OrPoseEstimatorStateToRobotState::or_t3d_acc_cov_to_accel_cov(const pom::or_t3d_acc_cov& or_t3d_acc_cov_msg, geometry_msgs::AccelWithCovariance& geometry_msgs_accel_covariance)
{
    // Full accel covariance matrix
    Eigen::Matrix<double, 6, 6> accel_covariance_mat;
    accel_covariance_mat.setZero();

    // Linear accel cov matrix
    Eigen::Matrix<double, 3, 3> linear_accel_covariance_mat;
    linear_accel_covariance_mat.setZero();

    // Low diagonal part of the linear accel cov mat
    int elem_i=0;
    for(int row_i=0; row_i<3; row_i++)
    {
        for(int col_i=0; col_i<=row_i; col_i++)
        {
            linear_accel_covariance_mat(row_i, col_i)=or_t3d_acc_cov_msg.cov[elem_i];
            elem_i++;
        }
    }

    // Set Full Lin Acc Matrix
    linear_accel_covariance_mat.triangularView<Eigen::Upper>() = linear_accel_covariance_mat.transpose();

    // Set Full Acc Matrix
    accel_covariance_mat.block<3,3>(3,3)=linear_accel_covariance_mat;

    // Convert to ROS Message
    double covarianceArray[36];
    Eigen::Map<Eigen::MatrixXd>(covarianceArray, 6, 6) = accel_covariance_mat;
    for(unsigned int i=0; i<36; i++)
    {
        geometry_msgs_accel_covariance.covariance[i]=covarianceArray[i];
    }


    return 0;
}
