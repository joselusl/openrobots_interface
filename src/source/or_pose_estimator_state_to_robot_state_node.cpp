
#include "openrobots_interface/or_pose_estimator_state_to_robot_state.h"


int main(int argc,char **argv)
{

    OrPoseEstimatorStateToRobotState MyOrPoseEstimatorStateToRobotState(argc, argv);
    std::cout<<"[ROSNODE] Starting "<<ros::this_node::getName()<<std::endl;

    MyOrPoseEstimatorStateToRobotState.open();

    MyOrPoseEstimatorStateToRobotState.run();

    return 0;
}

